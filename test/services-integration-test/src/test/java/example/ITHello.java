package example;

import com.caglabs.goldenzone.helloservice.client.Hello;
import com.caglabs.goldenzone.infomodel.dbloader.AbstractDBLoaderBase;
import com.caglabs.goldenzone.infomodel.dbloader.DBUtil;
import com.caglabs.goldenzone.infomodel.dbloader.ExampleInfoModelDBLoader;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.util.Properties;

import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.Assert.assertThat;

/**
 * Test the A service.
 */
public class ITHello {
    protected Hello doLookupJBossCheckWhyThisDoesntWorkOnGlassfish() throws NamingException {
        final Properties jndiProperties = new Properties();
        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        final Context context = new InitialContext(jndiProperties);

        // The JNDI lookup name for a stateless session bean has the syntax of:
        // ejb:<appName>/<moduleName>/<distinctName>/<beanName>!<viewClassName>
        //
        // <appName> The application name is the name of the EAR that the EJB is deployed in
        //           (without the .ear).  If the EJB JAR is not deployed in an EAR then this is
        //           blank.  The app name can also be specified in the EAR's application.xml
        //
        // <moduleName> By the default the module name is the name of the EJB JAR file (without the
        //              .jar suffix).  The module name might be overridden in the ejb-jar.xml
        //
        // <distinctName> : AS7 allows each deployment to have an (optional) distinct name.
        //                  This example does not use this so leave it blank.
        //
        // <beanName>     : The name of the session been to be invoked.
        //
        // <viewClassName>: The fully qualified classname of the remote interface.  Must include
        //                  the whole package name.
        String name = "ejb:app-ear-1.0-SNAPSHOT/hello-service/HelloBean!" + Hello.class.getName();
        System.out.println("Looking up: " + name);
        return (Hello) context.lookup(name);
    }

    protected Hello doLookup() throws NamingException {
        final Context context = new InitialContext();
        String name = "java:global/goldenzone/hello-service/HelloBean!" + Hello.class.getName();
        System.out.println("Looking up: " + name);
        return (Hello) context.lookup(name);
    }

    @Before
    public void setup() throws AbstractDBLoaderBase.DBLoadException {
        // Ensure database is populated
        new ExampleInfoModelDBLoader().execute();
    }

    @After
    public void teardown() {
        // Ensure database is dropped
        DBUtil.dropDB();
    }

    @Test
    public void testA() throws NamingException, Hello.HelloException {
        Hello hello = doLookup();
        String greeting = hello.sayHello("kalle");
        assertThat(greeting, startsWith("Hello Kalle Banan"));
    }
}
